require 'swagger_helper'

RSpec.describe 'api/post', type: :request do
    path '/api/v1/posts/{id}' do
        get 'Get posts detail' do
          tags 'Posts'
          security [bearerAuth: []]
          produces 'application/json'
          parameter name: :id, in: :path, type: :integer, required: true
  
          response '200', 'Successfully' do
            schema type: :object,
                   properties: {
                     posts: {
                       type: :object,
                       properties: {
                         id: { type: :integer },
                         title: { type: :string },
                         content: { type: :string },
                       }
                     }
                   }
  
            let(:id) { Post.create(title: "De men phieu luu ky", content: "De men...") }
            run_test! do |response|
              data = JSON.parse(response.body)['posts']
              news = Post.first
              expect(data['id']).to eq(news.id)
            end
          end
        end
      end
end
